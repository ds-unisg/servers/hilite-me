#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2009-2011 Alexander Kojevnikov <alexander@kojevnikov.com>
# Copyright © 2021 Bernhard Bermeitinger <bernhard.bermeitinger@unisg.ch>
#
# This code is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This code is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with hilite.me.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from functools import cmp_to_key
from urllib.parse import quote, unquote

from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles
from quart import Quart, render_template, request
from quart.helpers import make_response

from .tools import compare, get_default_css, hilite_me

__version__ = "v1.1.0"


_LEXERS = sorted(
    [(x_ids[0], x_name) for x_name, x_ids, *_ in get_all_lexers() if len(x_ids) > 0],
    key=cmp_to_key(lambda a, b: compare(a[1].lower(), b[1].lower())),
)
_STYLES = sorted(get_all_styles(), key=str.lower)


app = Quart(__name__)


@app.route("/", methods=["GET", "POST"])
async def index():
    data = await request.form

    cookies = request.cookies

    code = data.get("code", """print("Hello, World!")""")
    lexer = data.get("lexer", "") or unquote(cookies.get("lexer", "python"))
    style = data.get("style", "") or unquote(cookies.get("style", "colorful"))

    if request.method == "GET":
        line_numbers = unquote(cookies.get("line_numbers", ""))
        copyable = unquote(cookies.get("copyable", ""))
    else:
        line_numbers = data.get("line_numbers", "")
        copyable = data.get("copyable", "")

    css = data.get("css", unquote(cookies.get("css", "")))
    css = css or get_default_css()

    html = hilite_me(code, lexer, {}, style, line_numbers, css, copyable)

    page = await render_template(
        "index.html",
        code=code,
        html=html,
        lexer=lexer,
        lexers=_LEXERS,
        style=style,
        styles=_STYLES,
        line_numbers=line_numbers,
        css=css,
        copyable=copyable,
        version=__version__,
    )
    response = await make_response(page)

    cookie_vars = {
        "lexer": lexer,
        "style": style,
        "line_numbers": line_numbers,
        "css": css,
        "copyable": copyable,
    }

    next_year = datetime.datetime.now() + datetime.timedelta(days=365)
    for name, var in cookie_vars.items():
        response.set_cookie(name, quote(var), expires=next_year, samesite="Strict")

    return response


if __name__ == "__main__":
    app.run()
